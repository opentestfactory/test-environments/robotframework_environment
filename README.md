# Robot Framework Environment

This project is part of the OpenTestFactory initiative.

It generates a Docker image usable for executing Robot Framework tests.

The [OpenTestFactory documentation](https://opentestfactory.org/extra/robot-framework-environment-image.html) describes how to use this image.
