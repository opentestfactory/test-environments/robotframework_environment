FROM ubuntu:jammy-20240911.1

ENV TZ=Europe/Paris
# skip tzdata configuration prompt (even if ubuntu)
ENV DEBIAN_FRONTEND=noninteractive

ENV NODE_MAJOR=20
ARG APT_USER
ARG APT_PWD
ARG APT_REPOSITORY

WORKDIR /home/otf
COPY /requirements /tmp/requirements

# Install Webkit, Firefox, Chrome dependencies and tools from apt-get-requirements-file.txt
# hadolint ignore=DL3008
# hadolint ignore=SC2046
RUN apt-get update && apt-get install -qqy ca-certificates \ 
    && echo "deb [trusted=yes] https://$APT_USER:$APT_PWD@$APT_REPOSITORY" >> /etc/apt/sources.list.d/nexus.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends $(grep -vE "^\s*#" /tmp/requirements/apt-get-requirements-file.txt  | tr "\n" " ") \
    && rm -rf /var/lib/apt/lists/*  \
# Install node20
    && mkdir -p /etc/apt/keyrings \
    && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list \
    && apt-get update && apt-get install nodejs -y \
    # Clean up
    && rm -f /etc/apt/sources.list.d/nexus.list

# Install OpenTestFactory and robotframework libraries in venv.
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV NODE_PATH=/usr/lib/node_modules

RUN python3 -m venv $VIRTUAL_ENV \
    && pip install  -r /tmp/requirements/pip-requirements-file.txt \
    && sleep 1  \
    && /opt/venv/bin/rfbrowser init \
    && npx playwright@1.45 install-deps

# Create otf and set rights for SSH connections
COPY environment /etc/environment
RUN groupadd -r otf && useradd -r -g otf -G audio,video otf \
    && mkdir -p /home/otf/Downloads \ 
    && sed -i 's/#PasswordAuthentication (yes|no)/PasswordAuthentication no/g' /etc/ssh/sshd_config \
    && sed -i 's/#PermitUserEnvironment no/PermitUserEnvironment yes/g' /etc/ssh/sshd_config \ 
    && echo "otf ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
    && mkdir /home/otf/.ssh && touch /home/otf/.ssh/authorized_keys \
    && chown -R otf:otf /home/otf/ \
    && usermod --shell /bin/bash otf 

# Add boot files 
COPY --chmod=755 /boot-scripts/ /home/otf
USER otf
WORKDIR /home/otf

EXPOSE 22

ENTRYPOINT  [ "bash", "-c", "./init.sh && sudo ./display.sh && sudo /usr/sbin/service ssh restart && sleep infinity" ]
