#!/bin/sh


set -e


cp seccomp_profile.json /var/lib/kubelet/seccomp/seccomp_profile.json


echo "Installed or updated seccomp profile(s)"